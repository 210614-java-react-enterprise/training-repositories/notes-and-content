# Collection Exercise - 29 June 2021

Spend an hour with your group, researching and discussing your topics. At the end of this time, each group will present their findings and lead a discussion on their topics.
Some suggestions:
For interfaces or classes, highlight some of the important methods.
For data structures:
    - How are they stored? 
    - How are different operations performed? 
    - When would this data structure be a good fit?
    - When would this data structure not be a good fit?
    - Take a look at the openjdk implementation
Show an example of using each data structure yourself, showing off characteristics unique to the data structure where applicable.

Group 1 
- Queues
- Deques
- ArrayDeque
- Collection Hierarchy

Group 2 
- ArrayList
- LinkedList
- iterators
- iterator interface 
- Collections class

Group 3
- Sets 
- Maps
- HashSet 
- HashMap

Group 4
- Trees
- Heap data structure
- TreeMap
- TreeSet

Git branching exercise:
- pull or clone the most recent version of the notes-and-content repo
- create a branch called "team-#" and your team number
- add any content/examples from your teams presentation today to the collection-exercises folder (preferably in a folder for your team)
- add and commit your changes
- push your new branch up to gitlab
- *if your team had multiple resources*, each team member will want to add their resources to the branch
    - after a team member pushes their content to the team branch, the next team member will need to pull the most up to date changes
    - then, on the same team branch, the next team member would add their changes and commit them, pushing the next commit with their content
- open a pull request to the main branch in our repo 
- I will review each teams PR and merge it into main