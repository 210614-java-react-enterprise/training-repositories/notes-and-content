package dev.McCoy.SetsExample;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        // write your code here
        /*
        Set (extends Collection Interface)(extends Iterable)
        A collection that contains no duplicate elements, and only allows for one null element.
        A set also has no particular order.
        Places additional stipulations to inherited Collection interface constructors, add, equals, and hashCode methods.
        Note:(Be careful using mutable objects or any object that could change equals comparisons. Also sets cannot contain itself as an element.)
        Attempting to add an ineligible element throws either an unchecked exception (typically NullPointerException or ClassCastException),
        or returns false depending on the implementation.
        Attempting to query for an ineligible element may throw an exception or return false;

        Methods:
        iterator()
        toArray()
        retainAll()
        hashCode()
        spliterator()
        add()
        contains()
        isempty()
        remove()
        size()
        retainsAll()
         */


        //How to create a Set
        //Since a set is an interface it cannot be instantiated;
        Set<String> hashSet = new HashSet<>();

        //adding to the set
        hashSet.add("Hello");
        hashSet.add("world");

        //print out the set
        System.out.println(hashSet);

        Set<String> hSet = new HashSet<>();

        //adding to the set
        hSet.add("world");
        hSet.add("Hello");

        //print out the set
        System.out.println(hSet);

        //method of Set in Java is used to get the hashCode value for this instance of the Set.
        //It returns an integer value which is the hashCode value for this instance of the Set.

        Set<Integer> arr = new HashSet<>();
        arr.add(1);
        arr.add(2);
        arr.add(3);
        arr.add(4);

        System.out.println("Set: " + arr);

        //Get the hashCode value
        System.out.println("HashCode value: " + arr.hashCode());

        //Get the hashcode for Hello world
        System.out.println("HashCode value for Hello world: " + hashSet.hashCode());
        System.out.println("HashCode value for world Hello: " + hSet.hashCode());

        //Iterator method is used to return an iterator of the same elements as the set.
        //The elements are returned in random order from what present in the set.

        //Creating an iterator
        Iterator value = hashSet.iterator();

        System.out.println("Iterator values are: ");
        while (value.hasNext()) {
            System.out.println(value.next());
        }

    }
}
