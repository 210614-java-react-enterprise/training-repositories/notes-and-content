package main.java.dev.conor;

import java.util.HashMap;

public class HashMapDriver
{
    public static void main(String[] args)
    {
        //HashMap is a generic implementation of Map.  So, it stores key-value pairs, of
        //  whatever kinds of objects you'd like.

        //Lets make a HashMap:
        HashMap<Integer, String> coolestCars = new HashMap<Integer,String>();

        //So, what good is it?  Well, in this example, we can just start storing all of the cars that we like.
        coolestCars.put(1,"Mazda Taiki");
        coolestCars.put(2,"Dodge Challenger");
        coolestCars.put(3,"Chrysler 300");
        coolestCars.put(4,"Your weird uncle's project car that smells kinda like\n" +
                "poorly steamed tomatoes, and you realize you don't even\n" +
                "know what a steamed tomato is, like why would you ever\n" +
                "even steam a tomato?  Like what is the point, it's already\n" +
                "super gooey, wouldn't that just get really gross? Nevermind,\n" +
                "this is above my pay grade to think about...\n" +
                ".........................................................");

        //Lets take a look at it!
        printHashMap(coolestCars);

        //We can also get a set of all of the keys OR all of the maps, using the following:
        //coolestCars.keySet();
        //coolestCars.entrySet();

        /////
        /////
        /////


        //The "put" method can also be used to replace an item at a particular index, as seen here.
        //coolestCars.put(2,"Corvette Stingray");
        //printHashMap(coolestCars);

        /////
        /////
        /////

        //You can easily check to see if a HashMap contains a particular key, OR a particular value.
        //  System.out.println("Have we chosen a number one car? Survey says: " + coolestCars.containsKey(1));
        //  System.out.println("The most swagtastic ride to ever exist, then, is: THE " + coolestCars.get(1).toUpperCase());
        //Note that the GET method retrieves the VALUE at the index specified by the key we give it.

        /////
        /////
        /////

        //Note that HashMaps permit null values, which we can see here:
        //coolestCars.put(4, null);
        //printHashMap(coolestCars);

        /////
        /////
        /////

        //Note also that HashMaps permit you to assign a null KEY:
        //coolestCars.put(null, "NULL CAR, super secret tech, it's gettin' real weird in here!");
        //System.out.println(coolestCars.get(null));
        //printHashMap(coolestCars);//This will break, with a null pointer exception

        /////
        /////
        /////

        //Note also that a HashMap CAN, unlike a set, contain duplicate values:
        /*
        for (int x=0; x<100; x++)
        {
            coolestCars.put(x,"Hyundai Elantra");
        }
        printHashMap(coolestCars);
        */

        /////
        /////
        /////

    }

    public static void printHashMap(HashMap<Integer,String> hashMap)
    {
        System.out.println();
        System.out.println();
        //Print out all the objects in the HashMap, just to be safe
        for (int x : hashMap.keySet())
        {
            System.out.println(hashMap.get(x) + " is at index " + x);
            System.out.println();
        }
    }
}
