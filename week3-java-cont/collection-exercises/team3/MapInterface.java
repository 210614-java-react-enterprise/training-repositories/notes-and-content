import java.util.HashMap;
import java.util.Map;

public class MapInterface {

    public static void main(String[] args) {
        /*
        Methods for map interface
        isEmpty()
        containsKey()
        containsValue()
        get()
        put()
        putAll()
        hashcode()
        clear()
        getOrDefault()
        merge()
         */
        int temp = 0;
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        map.put('a', 1);
        map.put('b', 2);
        map.put('c', 3);

        System.out.println("Map is empty: "+map.isEmpty()+
                "\nMap contains the key 'a': "+map.containsKey('a')+
                "\nMap contains the value 7: "+map.containsValue(7)+
                "\nRetrieve value based on key 'a': "+map.get('a'));

        System.out.println("\nPutting a keypair value in will replace the old value if the key is the same.");
        map.put('a', 15);
        System.out.println("Retrieve value on updated key 'a': "+map.get('a'));
        //
        //
        Map<Character, Integer> map2 = new HashMap<Character, Integer>();
        //copies a map to another map (map2)
        map2.putAll(map);

        //below is viewing a keyset, which is a 'set' view of the keys contained
        System.out.println("\nBelow shows: " +
                "\n1)keySet on the new map with copied values - "+map2.keySet()+
                "\n2)values of the new map - "+map2.values()+
                "\n3)entrySet of the new map with copied values - "+map2.entrySet());

        //hashcode - sum of hash codes of each entry in the map
        System.out.println(map2.hashCode());
        System.out.println("map2.equals(map): "+map2.equals(map));
        //clears all values on a map
        map.clear();
        System.out.println("map2.equals(map): "+map2.equals(map));
        //stores map's value when changing, if key has no current value it will store the value you're adding
        int j = map2.getOrDefault('a', 50);


        HashMap<Integer, String> map3 = new HashMap<>();
        map3.put(1, "A");
        map3.put(2, "B");
        map3.put(3, "C");
        map3.put(5, "E");

        HashMap<Integer, String> map4 = new HashMap<>();
        map4.put(1, "G");   //It will replace the value 'A'
        map4.put(2, "B");
        map4.put(3, "C");
        map4.put(4, "D");   //A new pair to be added

        //Merge maps
        map4.forEach(
                (key, value) -> map3.merge( key, value, (v1, v2) -> v1.equalsIgnoreCase(v2) ? v1 : v1 + "," + v2)
        );

        System.out.println(map3);
    }

}
