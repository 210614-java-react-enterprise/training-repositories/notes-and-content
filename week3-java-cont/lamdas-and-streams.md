# Functional Programming in Java

As we know, Java is an object oriented programming language. However, since Java 8 we've had the tools to implement more functional style programming, using lambdas and the Stream API. 


## Lambdas 

A lambda is an anonymous inline implementation of a functional interface using arrow notation.
- _anonymous inline implementation_: defining the implementation of an interface without the need for defining an entirely new class in your program
- _functional interface_: a functional interface is an interface with only one abstract method
- _arrow notation_: arrow notation is a way to abbreviate method declarations
    - `()->{System.out.print("Hello World")}`
    - `(x, y) -> x+y`
    - `(int x) -> {return 5*x}`

Lambdas are oftentimes used as parameters of high order functions, meaning functions that take and/or return other functions. 

Let's take a look at an example: 

The `forEach` [method](https://docs.oracle.com/javase/8/docs/api/java/lang/Iterable.html#forEach-java.util.function.Consumer-) is defined in the `Iterable` interface. This method performs the given action to each element of the Iterable. The `forEach` method takes a parameter of type `Consumer`, and the `Consumer` is what defines the "given action." 

If we take a look at the [Consumer's documentation](https://docs.oracle.com/javase/8/docs/api/java/util/function/Consumer.html), we see it contains two methods - one having a default implementation, the other being abstract. Traditionally thinking, if we were to create a Consumer, to pass into the `forEach` method, we would need to create a class implementing the abstract `accept` method. However, this seems like a lot for likely very little code we would need to implement in this implementing class.

Without even using a lambda, we can create a Consumer without needing to create a whole other class. We can use an anonymous class to define an inline implementation: 

```java
        List<String> topics = Arrays.asList("Core Java", "SQL", "DevOps", "Agile");

        Consumer<String> printTopics = new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        };
        
        topics.forEach(printTopics);
```

Even still, this feels like a lot of work just to define the action to perform for each element. When we create a lambda, we are implementing the `accept` method inline, without the need for the local anonymous class definition. Because the `accept` method takes one parameter and has a void return, our lambda must take one parameter and not return anything. Refactoring the printTopics Consumer with a lambda:

```java
        List<String> topics = Arrays.asList("Core Java", "SQL", "DevOps", "Agile");

        Consumer<String> printTopics = (String s)->{System.out.println(s)};
        
        topics.forEach(printTopics);
```

Or even declare the lambda just as the method parameter if we don't plan on reusing the parameter:

```java
        List<String> topics = Arrays.asList("Core Java", "SQL", "DevOps", "Agile");
        
        topics.forEach(s->System.out.println(s));
```

Note how much more concise this is. Declaring the datatype for the lambdas parameters are optional, as they are inferred from the functional interface. When there is only one parameter, the parentheses around it are optional. The curly brackets for the body of the lambda are also optional when the return type is void. If the return type is not void and the braces are not included, the body of the lambda is implied to be returned. For example with `(x,y)->x+y`, `x+y` is implicitly returned.

In the case that we are only calling upon only another method's functionality in our lambda, we can actually simplify this expression even further.  We can usa **method reference**. With a method reference, we're essentially telling our program to "apply" a method to a particular input. In this case we would be telling the program to apply the `println` method from the `System` class' parameter `out` to the string parameter. We would reference this method `System.out::println` making our code even more succinct:

```java
        List<String> topics = Arrays.asList("Core Java", "SQL", "DevOps", "Agile");
                
        topics.forEach(System.out::println);

```

## Stream API

Lambdas and Streams were designed to work together. A stream is a sequence of elements supporting sequential or parallel aggregate operations. We generally will use streams to process data. Streams themselves do not store any data, we can just think of them as a pipeline for our data. Any pipeline can be made up of three parts:
1. (1) stream generator 
    - A stream generator converts some data source into a collection. This is commonly a collection or an array, but can also be a generator function, an I/O channel, etc.
2. (0 or more) intermediate stream operations
    - An intermediate stream operation is an operation which transforms a stream into another stream. These are operations like `filter`, `map`, `peek`, etc.
3. (1) terminal stream operation
    - A terminal stream terminates the pipeline, and returns some result or side effect. These are operations like `findFirst`, `count`, `forEach`, `reduce`, `collect`, etc.

### Taking a look at some examples

```java
List<Person> persons =
    Arrays.asList(
        new Person("Max", 18),
        new Person("Peter", 23),
        new Person("Pamela", 23),
        new Person("David", 12));

List<Person> filtered =
    persons
        .stream()
        .filter(p -> p.name.startsWith("P"))
        .collect(Collectors.toList());

System.out.println(filtered);    // [Peter, Pamela]
```

```java
Arrays.stream(new int[] {1, 2, 3})
    .map(n -> 2 * n + 1)
    .average()
    .ifPresent(System.out::println);  // 5.0
```


```java
List<String> myList =
    Arrays.asList("a1", "a2", "b1", "c2", "c1");

myList
    .stream()
    .filter(s -> s.startsWith("c"))
    .map(String::toUpperCase)
    .sorted()
    .forEach(System.out::println);
```



