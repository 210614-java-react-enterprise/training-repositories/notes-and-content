# XML - eXtensible Markup Language 
* like HTML, not a programming language but a markup language
* designed to transport and store data in a way that is both human and machine readable
* language agnostic

```XML 
<?xml version="1.0" encoding="UTF-8"?>
<students>
    <student>
        <firstName>Veronica</firstName> <lastName>Jones</lastName>
    </student>
    <student>
        <firstName>Paul</firstName> <lastName>McCormick</lastName>
    </student>
    <student>
        <firstName>Lola</firstName> <lastName>Nunez</lastName>
    </student>
</students> 
``` 

```JSON
"{"student":[
    { "firstName":"Veronica", "lastName":"Jones" },
    { "firstName":"Paul", "lastName":"McCormick" },
    { "firstName":"Lola", "lastName":"Nunez" }
]}"
```
JSON has been gaining popularity over XML for representing data because:
- faster parsing
- more compatible w javascript
- less verbose


<br>

#### Well-Formed vs. Valid XML

<hr>

| Well Formed XML | Valid XML |
| --------------- | --------- |
| Follows basic syntactic rules <ul><li>begins with XML declaration</li><li>unique root element</li><li>starting and ending tag must match</li><li>elements are case sensitive</li><li>elements must be properly nested</li></ul> Well formed document is not necessarily valid | XML which follows a predefined structure  <ul><li>Document Type Definition</li><li>XML Schema Definition</li></ul> Valid XML will also be well formed | 

<br>

#### XML Namespace

<hr>

- anyone can create their own markup with their own tags
- namespace allows us to differentiate from people using the same tag name for different purposes
- namespace declaration xmlns:prefix = "namespace"
- unique identifier URI - so if you have a registered URL you can associate it with your namespace to prevent naming conflict
- URL doesn't necessarily have anything to do with the namespace itself, just a way to prevent name clashes

```XML 
<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs=".../xmlSchema">
<employees>
    <employee>
        <firstName>John</firstName> <lastName>Doe</lastName>
    </employee>
    <employee>
        <firstName>Anna</firstName> <lastName>Smith</lastName>
    </employee>
    <employee>
        <firstName>Peter</firstName> <lastName>Jones</lastName>
    </employee>
</employees> 
``` 

