
--------------------------------------
-- using dml/dql to access data
--------------------------------------

select *
from movie;

select movie_name, duration
from movie;

select *
from movie
where duration > 150;

select movie_name, release_year
from movie 
where duration > 150;

select round(avg(duration),2)
from movie;

select avg(duration), release_year 
from movie
group by release_year
order by release_year desc;

update movie 
set duration = 125, release_year = 2001
where movie_name = 'spirited away';


select avg(duration), release_year
from movie
where release_year > 2000
group by release_year
having avg(duration) > 125
order by release_year desc;