
select *
from movie
join movie_actor
on movie.id = movie_actor.movie_id; 

select * 
from actor;

-- this allows us to select all columns from all 3 of the tables
select *
from movie
join movie_actor
on movie.id = movie_actor.movie_id
join actor
on actor.id = movie_actor.actor_id;

-- this join is implicitly an inner join, the inner keyword can be used but there will be no difference
select movie_name, actor_name
from movie
join movie_actor 
on movie.id = movie_actor.movie_id
join actor
on actor.id = movie_actor.actor_id;

-- if we wanted to display all of the actors and the movies they're in (if they are in any of the displayed movies or not)
select movie_name, actor_name
from movie
join movie_actor 
on movie.id = movie_actor.movie_id
right join actor
on actor.id = movie_actor.actor_id;

-- getting all actors and all movies 
select movie_name, actor_name
from movie
full join movie_actor 
on movie.id = movie_actor.movie_id
full join actor
on actor.id = movie_actor.actor_id;

-- getting all actors and movies where they apply
select movie_name, actor_name
from movie
join movie_actor 
on movie.id = movie_actor.movie_id
full join actor
on actor.id = movie_actor.actor_id;

-- get the actors that star in the longest movie
select *
from movie
join movie_actor
on movie.id = movie_actor.movie_id
join actor
on actor.id = movie_actor.actor_id
order by duration desc;

select *
from movie
order by duration desc;

select max(duration)
from movie;

select actor_name, movie_name 
from movie
join movie_actor
on movie.id = movie_actor.movie_id
join actor
on actor.id = movie_actor.actor_id
where duration = (
	select max(duration)
	from movie
)

create schema project0;
