
--- creating a view for all actors and their movies
create view all_actors_roles as 
select movie_name, actor_name
from movie
join movie_actor 
on movie.id = movie_actor.movie_id
full join actor
on actor.id = movie_actor.actor_id;

select * from all_actors_roles;

select * from (
	select movie_name, actor_name
	from movie
	join movie_actor 
	on movie.id = movie_actor.movie_id
	full join actor
	on actor.id = movie_actor.actor_id;
)

create materialized view all_actors_roles_materialized as 
select movie_name, actor_name
from movie
join movie_actor 
on movie.id = movie_actor.movie_id
full join actor
on actor.id = movie_actor.actor_id;


select * from all_actors_roles;
select * from all_actors_roles_materialized;

insert into movie values (default, 'Gangs of New York', 2002, 170);
insert into movie_actor values (12,1);
insert into movie_actor values (12,3);

refresh materialized view all_actors_roles_materialized;
