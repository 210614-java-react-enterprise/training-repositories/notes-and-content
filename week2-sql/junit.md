# JUnit

- unit test our code
- a unit test focuses on a really small potion of the codebase: using JUnit, a single method
- tests should not care what order they are run in

### Test Driven Development (TDD)

- write unit tests first
- watch them fail
- write code to satisfy tests

## JUnit Annotations

- `@Test` - unit test
- `@Disabled` - ignore a unit test
- `@BeforeEach` - runs before each unit test
- `@BeforeAll` - runs **once** before all unit tests in the class
- `@AfterEach`
- `@AfterAll`


## JUnit Assert class

- [docs](https://junit.org/junit5/docs/5.0.1/api/org/junit/jupiter/api/Assertions.html)
- Many useful static methods for determining the condition for which a test passes 
  - `assertEquals`
  - `assertTrue`
  - `assertFalse`
  - `assertArrayEquals`
  - `assertAll` - takes in a variable amount of Executable arguments, allows us to execute multiple assert methods in the same test, while still getting feedback from all of our tests


**`assertEquals`**

```java
int expected = 0;
int actual = someMethod();

Assert.assertEquals( expected, actual);
```

## Mocking Objects

Objects become difficult to test when they have many dependencies on other objects. If we are writing a test for the dependent object - which has a method which can call many other methods - it can be very difficult to figure out the cause of a problem when we encounter one. If we are able to isolate a particular method's functionality, it becomes much easier to test. We can employ strategies such as creating mocks and spies to try to do this.


### Example - Mocking an Object Ourselves

in EmployeeService.java
```java
public class EmployeeService {

    private EmployeeDao ed = new EmployeeDaoImpl();

	public boolean createEmployee(Employee e) {
		int employeesCreated = employeeDao.createEmployee(e);
		if(employeesCreated!=0) {
			return true;
		}
		return false;
	}
}
```

in EmployeeServiceTest.java 
```java
public class EmployeeServiceTest {

    EmployeeService employeeService = new EmployeeService();

    @Test
    public void testCreateEmployee(){
        assertTrue(employeeService.createEmployee(new Employee()));
    }

    @Test
    public void testCreateNullEmployee(){
        assertFalse(employeeService.createEmployee(null));
    }
}
```

In this first instance, we're unable to test the EmployeeService without also using the implementation of the DAO.

<hr>

```java
public class EmployeeService {

    private EmployeeDao employeeDao;

    public EmployeeService(EmployeeDao employeeDao){
        super();
        this.employeeDao = employeeDao;
    }

    public boolean createEmployee(Employee e) {
        int employeesCreated = employeeDao.createEmployee(e);
        if(employeesCreated!=0) {
            return true;
        }
        return false;
    }
}
```

```java
public class EmployeeServiceTest {

	//here we create a local anonymous class which takes the place of the typical jdbc dao to create one with a dummy method
    EmployeeDao employeeDao = new EmployeeDao() {
        @Override
        public int createEmployee(Employee e) {
            return e==null?0:1;
        }
    };
    EmployeeService employeeService = new EmployeeService(employeeDao);

    @Test
    public void testCreateEmployee(){
        assertTrue(employeeService.createEmployee(new Employee()));
    }

    @Test
    public void testCreateNullEmployee(){
        assertFalse(employeeService.createEmployee(null));
    }

}
```
Using this approach, we can inject the dependencies to the class we're testing at runtime. This means we can inject a different dependency when we're writing unit tests than we have when our application is running normally.

## Mockito

Mockito is a tool built expressly for creating mocks and spies for testing. A mock is basically a dummy object which we can add functionality to by stubbing its methods - a mock's default behavior is to do nothing. A spy is a real object whose methods we are able to override with custom implementation - their default behavior is that of the real object itself.

```java

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {

	@InjectMocks
	private EmployeeService employeeService;
	
	@Mock
	private EmployeeDao employeeDao;

	@Test
	public void testSuccessfulCreation() {
		Employee newEmployee = new Employee(5, "Joe", 500, "Intern", 2, new Department(2));
		when(employeeDao.createEmployee(newEmployee)).thenReturn(1);
		assertTrue(employeeService.createEmployee(newEmployee));
	}
	
	@Test
	public void testUnsuccessfulCreation() {
		Employee newEmployee = null;
		when(employeeDao.createEmployee(newEmployee)).thenReturn(0);
		assertFalse(employeeService.createEmployee(newEmployee));
	}

}
```

