-----------------------------
-- using ddl commands to create our tables
-----------------------------

create table movie(
	id serial primary key, 
	movie_name varchar(100),
	release_year integer,
	duration integer
);

create table actor(
	id serial primary key,
	actor_name varchar(100)
);

/* movie_actor is a junction table (or join table) */
create table movie_actor(
	movie_id integer references movie,
	actor_id integer references actor,
	primary key(movie_id, actor_id)
);

drop table movie_actor;
drop table actor;
drop table movie;


---------------------------
-- using dml to populate our tables
---------------------------

insert into movie values (default, 'inception', 2010, 165);
insert into movie (movie_name, release_year, duration) values ('titanic', 1997, 210);
insert into movie (release_year) values (2019);
insert into movie values (default, 'pulp fiction', 1994, 178);
insert into movie values (default, 'back to the future', 1985, 116);
insert into movie values (default, 'the lion king', 1994, 89);
insert into movie values (default, 'interview with the vampire', 1994, 123);
insert into movie values (default, 'titanic II', 2010, 90);

update movie 
set movie_name = 'Star Wars: Rise of Skywalker', duration = 142
where id = 4;

update movie 
set duration = 142
where id = 3;

delete from movie
where id = 3;

insert into actor values (1, 'Leonardo DiCaprio');
insert into actor (actor_name) values ('Joseph Gordon-Levitt');
insert into actor (actor_name) values ('Daniel Day-Lewis');
insert into actor (actor_name) values ('Mark Hamill');
insert into actor (actor_name) values ('Kate Winslet');

insert into movie_actor values (1, 1);
insert into movie_actor values (1, 2);

insert into movie_actor values (4, 4);
insert into movie_actor values (2, 1);
insert into movie_actor values (2, 5);

insert into movie (movie_name) values ('spirited away');

delete from actor 
where actor_name = 'spirited away';

----------------------------------------
-- using ddl to alter the tables
----------------------------------------
alter table movie alter column movie_name set not null;

alter table actor add column birthdate date;

truncate table movie_actor;