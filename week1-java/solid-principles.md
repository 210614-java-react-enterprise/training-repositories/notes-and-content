## SOLID Principles 

The SOLID principles are a set of principles which are applied to object oriented design to create more maintainable, understandable, and flexible.

- **Single Responsibility**
- **Open/Closed**
- **Liskov Substitution**
- **Interface Segregation**
- **Dependency Inversion**

### Single Responsibility Principle
The Single Responsibility Principle states that a class should have one responsibility. This also means that it should only have one reason to change. This makes our code:
- more modular
- more loosely coupled 
- simpler to test
- better organized


### Open/Closed Principle
The Open/Closed Principle states that classes should be open for extension but closed for modification. This means we should be able to extend their functionality without needing to change the code. This allows our code to be more maintainable and mitigates the risk of introducing new bugs into our prior functional code.

This code does not adhere to the open closed principle. If you wanted to add a new Garment, you would have to modify this method.
```java
    public void foldClothing(Garment[] garments){
        for(Garment garment: garments){
            if(garment.getClass().equals(Shirt.class)){
                System.out.println("folding shirt");
            } else if(garment.getClass().equals(Pants.class)){
                System.out.println("folding pants");
            } else if(garment.getClass().equals(Socks.class)){
                System.out.println("folding socks");
            }
        }
    }
```

Instead, if we had all of our Garments implementing their own fold method, our code would look like this:
```java
    public void foldClothing(Foldable[] foldables){
        for(Foldable foldable: foldables){
            foldable.fold();
        }
    }
```

```java
    // in shirt class
    public void fold(){
        System.out.println("folding shirt");
    }
```

```java
    // in pants class
    public void fold(){
        System.out.println("folding pants");
    }
```

```java
    // in socks class
    public void fold(){
        System.out.println("folding socks");
    }
```

In this second version, the Open/Closed principle is upheld. The foldClothing implementation is open to extension by creating other Foldable items, but foldClothing need not be modified to do so.

### Liskov Substitution Principle

The Liskov Substitution Principle states that if A is a superclass of B, then the application should function properly if any instance of B were to replace A.

Let's take a look at an example where this is not the case:

```java
    class Rectangle {
        void setWidth(double w){...}
        void setHeigh(double h){...}
        double getWidth(){...}
        double getHeight(){...}   
    }

    class Square extends Rectangle {
        void setWidth(double w){ /* set both height and width to w */ }
        void setHeigh(double h){ /* set both height and width to h */ }
        double getWidth(){...}
        double getHeight(){...} 
    }
```

If we had code which set the side lengths and calculated the area, we should expect a Square object to take the place of the Rectangle object without any issue. However, the code below would execute differently for each:

```java
    void testRectangle(Rectangle r){
        r.setWidth(4);
        r.setHeight(5);
        System.out.println("Rectangle area: " + (r.getWidth()*r.getHight()));
    }
```

When a rectangle is passed in we get 20, but when a square is passed in we get 25, thus 

### Interface Segregation Principle
The interface segregation principle states that we should divide interfaces into the smallest possible units, so as to not force any classes to implement interfaces they don't need. Let's say, instead of the Foldable interface we created in our Garment example, we created a Tailor interface. The Tailor interface included the fold method, but it also included a hem method, as well as a press method. This would mean that no object could implement just the fold method alone - and there are plenty of objects that may not need all three methods. This principle keeps us from creating bloated objects and keeps implementation simpler and more relevant.

### Dependency Inversion
The Dependency Inversion principle states that high-level modules should not depend on low-level modules. Instead, they should both depend on abstraction. What this means is that a dependency should be defined as an interface or an abstract class when possible.

In this first class, we tightly couple together the implementation of the Invoice and the HashMap implementation of the Map interface. If we ever wanted to change the implementation of the map or how we handle purchased garments, we would not have much flexibility. We would likely need to do quite a bit of refactoring.

```java
public class Invoice {
    
    private HashMap<Garment, Integer> purchasedGarments = new HashMap<>();

    public Invoice(){
        super();
    }

}
```

How can we more loosely couple these dependencies? If we have the Invoice class depend on the Map interface, rather than the HashMap implementation, we have the flexibility to provide any implementation of the Map interface. And if we do change how we handle the purchased garments down the line, the Invoice class will never need to be changed.

```java
public class Invoice {
    
    private Map<Garment, Integer> purchasedGarments;

    public Invoice(Map purchasedGarments){
        super();
        this.purchasedGarments = purchasedGarments;
    }

}
```


