# AWS

## EC2 - Elastic Compute Cloud

- web service that provides secure, scalable compute capacity in the cloud
- follows the infrastructure as a service (IaaS) model

#### AMI - Amazon Machine Image

- template for your virtual server
- includes O/S + any other initial software
- can create custom AMIs

#### EBS - Elastic Block Storage

- configurable block storage that can be attached to an EC2
- root volume comes pre configured, but additional volumes of EBS can be attached

#### Security Groups

- allows protocol and port level security configuration
- can set rules that filter traffic coming in/out of your EC2 instance

#### Autoscaling groups

- can configure EC2 to be a part of autoscaling groups
- group scales up or down as needed (horizontal scaling)
- [vertical vs horizontal scaling](https://www.section.io/assets/images/blog/featured-images/horizontal-vs-vertical-scaling-diagram.png)

#### Elastic Load Balancing (ELB)

- distributes incoming traffic across multiple AWS resources automatically
- send requests to a load balancer URL, and the requests are distributed appropriately
  <img src="https://media.amazonwebservices.com/blog/2014/elb_instances_1.png" alt="elastic load balancing">

### Connecting to an EC2

> ssh -i [private key] ec2-user@[ip or endpoint]

## [AWS Global infrastructure](https://infrastructure.aws/)

## S3

### i. object storage

- object name + bucket + version ID = unique identifier
- object = content + metadata
- 5+ Terabytes
- durable and available
  - 99.999999999%
  - This durability level corresponds to an average annual expected loss of 0.000000001% of objects. For example, if you store 10,000,000 objects with Amazon S3, you can on average expect to incur a loss of a single object once every 10,000 years.
  - automatically distributed across a minimum of three physical availability zones
- general purpose, infrequent access, glacier -> all durable, just less available
  - can configure lifecycle
- only pay for what you use

### ii. static web hosting

- we can host static web pages for public access

## IAM

- allows you to define and allocate user based permissions in AWS
- can allocate access to subsets of services within aws and assign them to users or roles
- allows you to set up MFA or federated access

## Cloud Computing Models

<img src="https://www.cloudindustry.com/wp-content/uploads/2017/06/Cloud-Computing-01.png" alt="cloud computing models">

<img src="https://miro.medium.com/max/1682/1*peN3l27025YUoY0FEqllVA.jpeg" alt="pizza as a service">
