# DevOps

- development (writing the code) + operations (maintaining the infrastructure to host your applications)
- devops combines these two processes to help us write better quality code more efficiently

### Continuous Integration

- regularly pushing your code into a shared repository, where tests are run and a build artifact is created automatically in order to determine if the new additions integrated successfully
- allows us to identify and resolve issues much more efficiently

### Continuous Delivery

- CI + manual deployment of build artifact

### Continuous Deployment

- CI + automatic deployment of build artifact

<img src="https://lh3.googleusercontent.com/proxy/0A1-mWDLOefIezmAfjLXJZS85rHgccwMSmywruXjjSZy70Q9YkfAVMQ0Swsn4LlPwN1UdUqIPp_Vxjjo3Z0pCryxp0ugBKLz35pZq_R4AKw1Yzr7skocApI-YNw" alt="ci/cd">
